package br.com.itau.cacaNiquel;

import java.util.ArrayList;
import java.util.List;


public class App 
{
    public static void main( String[] args )
    {
        Jogo jogo = new Jogo();
        List<Slot> slots = new ArrayList<Slot>();
        slots = jogo.Jogar();
        jogo.calculaPonto(slots);
        
        System.out.println(slots);
        System.out.println("" + jogo.getTotal());
    }
}
