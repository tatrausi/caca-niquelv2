package br.com.itau.cacaNiquel;

public enum Simbolo {
	BANANA(10), ABACAXI(20), JABOTICABA(50), SETE(100);

	private int pontuacao;

	private Simbolo(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public int getPontuacao() {
		return pontuacao;
	}

}
