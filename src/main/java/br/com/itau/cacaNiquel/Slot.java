package br.com.itau.cacaNiquel;

public class Slot {

	private Simbolo simbolo;
	
	public Slot(Simbolo simbolo) {
		this.simbolo = simbolo;
	}
	
	public Simbolo getSimbolo() {
		return simbolo;
	}
	
	@Override
	public String toString() {
		return "" + simbolo;
	}
	
}
