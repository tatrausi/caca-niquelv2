package br.com.itau.cacaNiquel;

import java.util.ArrayList;
import java.util.List;

public class Jogo {
	private int qtdSlot = 3;
	private int total = 0;
	private Sorteador sorteador = new Sorteador();
	private Simbolo simbolo;
	
	public List<Slot> Jogar() {
		List<Slot> slots = new ArrayList<Slot>();
		for (int i = 0; i < qtdSlot; i++) {
			Slot slot = sorteador.Sortear();
			slots.add(slot);
			total += slot.getSimbolo().getPontuacao();
		}
		return slots;
	}
	
	public int calculaPonto(List<Slot> slots) {
		
		if(slots.get(0) == slots.get(1) && slots.get(0) == slots.get(2)) {
			if(slots.get(0).equals(Simbolo.SETE)) {
				total = total * 5;
			}else {
				total = total * 3;
			}
		}else {
			if((slots.get(0) == slots.get(1)) || (slots.get(0) == slots.get(1)) ||
					(slots.get(1) == slots.get(2))) {
				total = total * 2;
			}
		}
		return total;
	}
	
	public int getTotal() {
		return total;
	}
	

}
