package br.com.itau.cacaNiquel;

import java.util.ArrayList;
import java.util.List;

public class Sorteador {
	
	private List<Slot> slots = new ArrayList<Slot>();
	
	public Sorteador() {
		for (Simbolo simbolo : Simbolo.values()) {
			Slot slot = new Slot(simbolo);
			slots.add(slot);
		}
	}
	
	public Slot Sortear() {
		  int posicao = (int) (Math.random() * slots.size());
		  return slots.get(posicao);
	}

}
